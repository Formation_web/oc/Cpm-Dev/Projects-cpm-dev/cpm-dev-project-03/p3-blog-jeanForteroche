<?php

namespace App\Backend\Modules\Users;

use \Entity\Users;
use \OCFram\BackController;
use \OCFram\HTTPRequest;
use \Form\FormHandler;
use \Form\FormBuilder\RegisterFormBuilder;

class UsersController extends BackController
{

	public function executeUpdate(HTTPRequest $request)
	{
		if( $request->method() == 'POST' )
		{
			$user = new Users( array(
				'username' => $request->postData( 'username' ),
				'password' => $request->postData( 'password'),
				'email' => $request->postData( 'email'),
			));

			if($user->isNew())
			{
				// On force le role utilisateur à USER
				$user->setRole('USER');
				// On génère une clé de salage
				$user->setSalt(substr(md5(time()), 0, 23));
			}

			$mdpForm = $request->postData('password');

			if(isset($mdpForm) && !empty($mdpForm))  {

				$pass = sha1($mdpForm . $user->salt());

				$user->setPassword($pass);

			} else {

				$erreurs = 'Veuillez entrez un mot de passe valide !';

				$this->app->httpResponse()->redirect( '/users/register-user.html' );

			}

		}
		else
		{
			$user = new Users();
		}


		$formBuilder = new RegisterFormBuilder($user);
		$formBuilder->build();
		$form = $formBuilder->form();

		$formHandler = new FormHandler($form, $this->managers->getManagerOf('Users'), $request);

		if ($formHandler->process())
		{
			$this->app->user()->setFlash( 'L\'utilisateur à bien été modifié !');
			$this->app->httpResponse()->redirect('/admin/home.html#users');
		}

		// Si une erreur a été générer, on l'envoie à la page
		if(isset($erreurs)) {
			$this->page->addVar( 'erreurs', $erreurs );
		}

        $this->page->addVar( 'user', $user );
		$this->page->addVar('form', $form->createView());

	}

	public function executeDelete(HTTPRequest $request)
	{
		$this->managers->getManagerOf('Users')->delete($request->getData('id'));

		$this->app->user()->setFlash('L\'utilisateur et ses commentaires ont bien été supprimer.');

		// redirection vers l'Admin
		$this->app->httpResponse()->redirect('/admin/home.html');
	}



	public function executeSwitchRole(HTTPRequest $request)
	{
		$manager = $this->managers->getManagerOf('Users');

		if ($request->getExists('id')) {
			$manager->switchUserRole($request->getData('id'));
			$this->app->user()->setAuthenticated(FALSE);
			$this->app()->user()->setFlash('Le rôle de l\'utilisateur à bien été changer');
			$this->app()->httpResponse()->redirect('/admin/connect.html');
		} else {
			$this->app->user()->setFlash('Aucun identifiant n\'a été transmis !!!');
			$this->app->httpResponse()->redirect404();
		}

		if ($_SESSION['role'] == 'USER') {
			return $_SESSION['role'] = 'ADMIN';
		} elseif ($_SESSION['role'] == 'ADMIN') {
			return $_SESSION['role'] = 'USER';
		}
	}

}