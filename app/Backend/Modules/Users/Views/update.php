
<div class="container">
    <div>
        <!-- Affichage des erreurs si il y en a-->
		<?php if (isset($erreurs)) : ?>
            <div class="row">
                <div class="alert alert-danger text-center">
                    <strong>Login failed</strong> <?= $erreurs ?>
                </div>
            </div>
		<?php endif; ?>



    <hr>
        <h1 class="text-center"><?= $title ?></h1>
    <hr>


    <form action="" method="post" class="form-horizontal well">

        <div class="row">

            <?= $form ?>

            <div class="form-group">
                <div class="col-sm-9 col-sm-offset-3">
                    <input type="submit" class="btn btn-success btn-block" value="Valider" />
                </div>
            </div>

        </div>

    </form>

</div>
