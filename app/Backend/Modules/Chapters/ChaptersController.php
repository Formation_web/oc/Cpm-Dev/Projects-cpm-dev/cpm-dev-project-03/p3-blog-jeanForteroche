<?php
namespace App\Backend\Modules\Chapters;

use \OCFram\BackController;
use \OCFram\HTTPRequest;
use \Entity\Chapters;
use \Form\FormHandler;
use \Form\FormBuilder\ChaptersFormBuilder;

class ChaptersController extends BackController
{
    // ADMIN Home Page
    public function executeAdmin( HTTPRequest $request )
    {
        if( $this->app->user()->isAuthenticated() && $this->app->user()->isAdmin() )
        {
            $chapterManager = $this->managers->getManagerOf( 'Chapters' );
            $commentManager = $this->managers->getManagerOf( 'Comments' );
            $userManager    = $this->managers->getManagerOf( 'Users' );

            $this->page->addVar( 'title', 'Administration du blog' );

            $this->page->addVar( 'nbChapters', $chapterManager->count() );

            $this->page->addVar( 'chaptersList', $chapterManager->getList() );

            $this->page->addVar( 'commentsList', $commentManager->getList() );

            $this->page->addVar( 'nbComments', $commentManager->count() );

            $this->page->addVar( 'nbCommentsSignaled', $commentManager->countCommentsSignaled() );

            $this->page->addVar( 'usersList', $userManager->getList() );
            $this->page->addVar( 'nbUsers', $userManager->count() );
        } else {
            $this->app->user()->setFlash( 'Désolé, vous n\'avez pas d\'autorisation pour accéder à l\'espace d\'administration' );
        }
    }



    // INSERT A CHAPTER
    public function executeInsert( HTTPRequest $request )
    {
        $this->processFormChapter( $request );

        $this->page->addVar( 'title', 'Ajout d\'un chapitre' );
    }

    // UPDATE A CHAPTER
    public function executeUpdate( HTTPRequest $request )
    {
        $this->processFormChapter( $request );

        $this->page->addVar( 'title', 'Modification d\'un chapitre' );
    }

    // DELETE A CHAPTER
    public function executeDelete( HTTPRequest $request )
    {
        if( $request->getExists( 'id' ) ) {
            $id = $request->getData( 'id' );

//         Suppresion de tous les commentaires lié au chapitre a supprimer
            $this->managers->getManagerOf( 'Comments' )->deleteAllByChapter( $id );
            // SUppression du chapitre demander
            $this->managers->getManagerOf( 'Chapters' )->delete( $id );

            $this->app->user()->setFlash( 'Le chapitre a bien été supprimée !' );

            $this->app->httpResponse()->redirect( '/admin/home.html' );
        } else {
            $this->app->user()->setFlash( 'Aucun identifiant de chapitre n\a été transmis !' );
        }
    }




     //Process Form //
    public function processFormChapter( HTTPRequest $request )
    {
        if( $request->method() == 'POST' ) {
            $chapter = new Chapters( [
                 'author' =>  'Jean Forteroche',
                 'title' => $request->postData( 'title' ),
                 'content' => $request->postData( 'content' ),
             ] );

            if( $request->getExists( 'id' ) ) {
				$chapter->setId( $request->getData( 'id' ) );
			}
        }
        else
        {
            if( $request->getExists( 'id' ) ) {
                $chapter = $this->managers->getManagerOf( 'Chapters' )->find( $request->getData( 'id' ) );
            }
            else
            {
                $chapter = new Chapters();
            }
        }

        $formBuilder = new ChaptersFormBuilder( $chapter );
        $formBuilder->build();
        $form        = $formBuilder->form();
        $formHandler = new FormHandler( $form, $this->managers->getManagerOf( 'Chapters' ), $request );

        if( $formHandler->process() ) {
            $this->app->user()->setFlash($chapter->isNew() ? 'Le chapitre a bien été ajoutée !' : 'Le chapitre a bien été modifiée !' );
            $this->app->httpResponse()->redirect( '/admin/home.html' );
        }

		// Si une erreur a été générer, on l'envoie à la page
		if(isset($erreurs)) {
			$this->page->addVar( 'erreurs', $erreurs );
		}

        $this->page->addVar( 'chapter', $chapter );
        $this->page->addVar( 'form', $form->createView() );
    }

}