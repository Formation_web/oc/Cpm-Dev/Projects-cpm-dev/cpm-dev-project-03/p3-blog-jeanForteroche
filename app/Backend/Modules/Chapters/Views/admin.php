<hr>
<h2 class="text-center" >Administration</h2>

            <!--Décommenter pour voir les valeurs des variables de sessions sur la page d'administration -->
<!--        <h3>--><?//= var_dump($_SESSION); ?><!--</h3>-->
<hr>

<!-- ************** NAV TABS ***************  -->
<div class="row">
    <div class="col-sm-6 col-sm-offset-3 col-md-10 col-md-offset-1">
        <ul class="nav nav-tabs nav-justified">

            <li><a href="#chapters" data-toggle="tab">Chapitres</a></li>

            <li><a href="#comments" data-toggle="tab">Commentaires</a></li>

            <li><a href="#users" data-toggle="tab">Utilisateurs</a></li>

        </ul>
    </div>
</div>

<?php if(isset($chaptersList)): ?>
<div class="tab-content">

    <!-- *************** SECTION CHAPITRES ***************-->

    <div class="tab-pane fade in active adminTable" id="chapters">

        <p class="introAdmin text-info">Il y a actuellement <?= $nbChapters ?> chapitre(s). En voici la liste :</p>
        <div class="table-responsive">

            <table class="table table-hover">

                <thead> <!-- Ligne de TITRE  DU TABLEAU -->
                <tr>
                    <th>Titre</th>
                    <th>Contenu</th>
                    <th>Auteur</th>
                    <th>Créer le</th>
                    <th>Modifié</th>
                    <th></th>  <!-- Actions column -->
                </tr>
                </thead>

				<?php foreach($chaptersList as $chapter): ?>
                    <tr>
                        <td><a href="../chapters/chapter-<?= $chapter->id() ?>.html"><?= htmlspecialchars(substr(nl2br($chapter->title()), 0, 12)).'...' ?></a></td>
                        <td><?= substr(nl2br($chapter->content()), 0, 15).'...' ?></td>
                        <td><?= htmlspecialchars(substr(nl2br($chapter->author()),0,6)).'.' ?></td>
                        <td><?= $chapter->dateCreate()->format('d/m/Y à H\hi') ?></td>
                        <td>
							<?php
							if($chapter->dateCreate() != $chapter->lastModif())
								echo $chapter->lastModif()->format('d/m/Y à H\hi');
							?>
                        </td>
                        <td><!-- Boutons d'action MODIFIER & SUPPRIMMER -->
                            <a href="/admin/chapter-update-<?= $chapter->id() ?>.html" class="btn btn-info" title="Modifier le chapitre d\'id = <?= $chapter->id() ?>"><span class="glyphicon glyphicon-edit"></span></a>

                            <a href="/admin/chapter-delete-<?= $chapter->id() ?>.html" class="btn btn-danger" title="Supprimer le chapitre d\'id = <?= $chapter->id() ?>"><span class="glyphicon glyphicon-remove"></span></a>
                        </td>
                    </tr>
				<?php endforeach; ?>

            </table><!-- /.table.table-hover -->

        </div><!-- /.table-responsive -->

		<?php else: ?>

            <div class="alert alert-warning">Aucun chapitre n'a été trouvée.</div>

		<?php endif; ?>

        <a  href="/admin/chapter-insert.html" class="btn btn-success center-block"><span class="glyphicon glyphicon-plus"></span> Ajouter un chapitre</a>

    </div><!-- #chapters /.tab-pane .fade .adminTable -->




    <!-- ********* SECTION COMMENTAIRES **********-->

    <div class="tab-pane fade adminTable" id="comments">

        <p class="introAdmin text-info">Il y a actuellement <?= $nbComments ?> commentaire(s), dont <?= $nbCommentsSignaled ?> commentaire(s) qui ont été signalé. En voici la liste :</p>

		<?php if(isset($commentsList)): ?>

            <div class="table-responsive">

                <table class="table table-hover">

                    <thead> <!-- LIGNE DE TITRE  DU TABLEAU -->
                    <tr>
                        <th>Chapitre</th>
                        <th>Auteur</th>
                        <th>Contenu</th>
                        <th>Signalement</th>
                        <th>Action</th>  <!-- Actions column -->
                    </tr>
                    </thead>

					<?php foreach ($commentsList as $comment): ?>

                        <tr <?php if($comment->flag() != 0) { ?> id="commentSignaledInAdmin"<?php } ?> >
                            <td class="text-center"><?= $comment->chapter(); ?></td>
                            <td><?= substr(nl2br($comment->author()),0,6).'.' ?></td>
                            <td><?= substr($comment->content(), 0, 15) . ' ...'; ?></td>
                            <td class="text-center"><?= $comment->flag(); ?></td>
                            <td><!-- Boutons d'action MODIFIER & SUPPRIMER -->
                                <a href="/admin/comment-update-<?= $comment->id() ?>.html" class="btn btn-info" title="Modifier le commentaire d\'id = <?= $comment->id() ?>"><span class="glyphicon glyphicon-edit"></span></a>

                                <a href="/admin/comment-delete-<?= $comment->id() ?>.html" class="btn btn-danger" title="Supprimer le comment d\'id = <?= $comment->id() ?>"><span class="glyphicon glyphicon-remove"></span></a>
                            </td>
                        </tr>

					<?php endforeach; ?>

                </table><!-- /.table.table-hover -->

            </div><!-- /.table-responsive -->

		<?php else:  ?>

            <div class="alert alert-warning">Aucun commentaire n'a été trouvée.</div>

		<?php endif; ?>

    </div><!-- #comments /.tab-pane .fade .adminTable -->


    <!--************* SECTION USERS *************-->

    <div class="tab-pane fade adminTable" id="users">

        <p class="introAdmin text-info" >Il y a actuellement <?= $nbUsers ?> utilisateur(s). En voici la liste :</p>

		<?php if(isset($usersList)): ?>

            <div class="table-responsive">

                <table class="table table-hover">

                    <thead><!-- LIGNE DE TITRE DU TABLEAU -->
                    <tr>
                        <th>id</th>
                        <th>Username</th>
                        <th>E-Mail</th>
                        <th>Rôle</th>
                        <th>Inscription</th>
                        <th></th>  <!-- Actions column -->
                    </tr>
                    </thead>

					<?php foreach ($usersList as $users): ?>
                        <tr>
                            <td class="text-center padding-table"><?= $users->id() ?></td>
                            <td class="text-center padding-table"><?= $users->username() ?></a></td>
                            <td class="text-center padding-table"><?= $users->email() ?></td>
                            <td class="text-center padding-table">
                                <a href="/admin/switch-role-<?= $users->id() ?>.html" class="btn btn-info btn-table"><span class="glyphicon glyphicon-share" title="Intervertit le rôle utilisateur entre USER & ADMIN"> <?= $users->role() ?></span></a></td>
                            <td class="text-center padding-table"><?= $users->inscription()->format('d/m/Y à H\hi') ?></td>
                            <td><!-- Boutons d'action MODIFIER & SUPPRIMER -->
                                <a href="/admin/user-update-<?= $users->id() ?>.html " class="btn btn-info" title="Modifier l\'utilisateur d\'id = <?= $users->id() ?>"><span class="glyphicon glyphicon-edit"></span></a>
                                <a href="/admin/user-delete-<?= $users->id()?>.html" class="btn btn-danger" title="Supprimer l\'utilisateur d\'id<?= $users->id(); ?>"><span class="glyphicon glyphicon-remove"></span></a>
                            </td>
                        </tr>
					<?php endforeach; ?>

                </table><!-- /.table.table-hover -->

            </div><!-- /.table-responsive -->

		<?php else: ?>

            <div class="alert alert-warning">Aucun utilisateur n'a été trouvée.</div>

		<?php endif; ?>

        <a class="btn btn-success center-block" href="/users/register-user.html"><span class="glyphicon glyphicon-plus"></span> Ajouter un utilisateur</a>

    </div><!-- #users /.tab-pane .fade .adminTable -->

</div><!-- /.tab-content -->

