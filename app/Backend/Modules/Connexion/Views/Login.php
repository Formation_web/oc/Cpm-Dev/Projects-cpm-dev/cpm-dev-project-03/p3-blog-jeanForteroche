<hr>
<h2 class="text-center"><?= $title; ?></h2>
<hr>

<!-- Affichage des erreurs si il y en a-->
<?php if (isset($erreurs)) : ?>
    <div class="row">
        <div class="alert alert-danger text-center">
            <strong>Login failed</strong> <?= $erreurs ?>
        </div>
    </div>
<?php endif; ?>


<form action="" method="post" class="form-horizontal well">

    <?= $form ?>

    <div class="form-group">
        <div class="col-sm-5 col-sm-offset-5">
            <input type="submit" class="btn btn-success btn-block" value="Connexion" />
        </div>
    </div>

</form>

