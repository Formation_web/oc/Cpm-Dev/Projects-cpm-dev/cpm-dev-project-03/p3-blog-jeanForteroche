<?php

namespace App\Backend\Modules\Connexion;

use \OCFram\BackController;
use \OCFram\HTTPRequest;
use \Entity\Users;
use \Form\FormBuilder\ConnectFormBuilder;


class ConnexionController extends BackController
{
    /**
     * LogIn Controller
     * @param HTTPRequest $request
     */
    public function executeLogin(HTTPRequest $request)
    {
        $this -> page -> addVar ( 'title' , 'Connexion' );

        $this->processFormLogin($request);
    }

    /**
     * Logout Controller
     * @param HTTPRequest $request
     */
    public function executeLogout(HTTPRequest $request)
    {
		$this->app->user()->setAuthenticated(false);

		// Redirection vers la homepage
        $this->app->httpResponse()->redirect('/');

        // On génère un message flash qui sera affiché à l'utilisateur
        $this->app->user()->setFlash('Merci de votre visite, à bientôt !');


    }

    /**
     * Gestion du formulaire de connexion
     * @param \OCFram\HTTPRequest $request
     */
    public function processFormLogin( HTTPRequest $request )
    {
        if( $request->method() == 'POST' ) {
            $user = new Users( [
               'username' => $request->postData( 'username' ),
               'password' => $request->postData( 'password' ),
               'email' => $request->postData( 'email' ),
           ] );

            $userBDD = $this->managers
	            ->getManagerOf('Users')
	            ->findByUsername($request->postData('username'));

            if(!empty($userBDD)) {
                // On récupère la clé de salage en BDD
                $salt = $userBDD['salt'];
                // On récupère le hashage en BDD
                $hash = $userBDD['password'];

                // On récupère le mdp du formulaire de connexion
                $pass = $request->postData('password');

                if($hash == sha1($pass . $salt))
                {
                    $user = $userBDD;

                    $this->app->user()->setAuthenticated(true);

                    $_SESSION['role'] = $userBDD->role();

                    $this->app->user()->setFlash('Connexion établie, bonne lecture !');
                    $this->app->httpResponse()->redirect('/');
                }
                else
                {
                    $erreurs = 'Votre mot de passe ne correspond pas!';
                }
            }
            else
            {
                $erreurs = 'Votre nom d\'utilisateur est incorrect !';
            }
        }
        else
        {
            $user = new Users();
        }

        $formBuilder = new ConnectFormBuilder($user);
        $formBuilder->build();
        $form        = $formBuilder->form();

        // Si une erreur a été générer, on l'envoie à la page
        if(isset($erreurs)) {
            $this->page->addVar( 'erreurs', $erreurs );
        }

        if(!empty($user)) {
            $this->page->addVar('user', $user);
        }
        // On envoie le formulaire à la page
        $this->page->addVar('form', $form->createView());

    }

}

