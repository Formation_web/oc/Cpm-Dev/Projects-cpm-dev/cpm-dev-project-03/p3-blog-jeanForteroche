
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />

        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta name="description" content="Blog avec une architecture MVC réalisée à l'occasion du Projet numéro 3 de la formation Chef de Projet Multimédia Option Dévelopment Web de la plate-forme Openclassrooms " />
        <meta name="author" content="Delzongle Joel" />

        <link rel="icon" href="<?= $this->app->config()->get('ROOT') . '/img/book.png' ?>" />

        <title>
            <?= isset($title) ? $title : 'Billet simple pour l\'Alaska' ?>
        </title>

        <!-- Bootstrap CSS -->
        <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.7/slate/bootstrap.min.css" rel="stylesheet" type="text/css" />

        <!-- My stylesheet of CSS -->
        <link href="<?= $this->app->config()->get('ROOT') . '/css/style.css' ?>" rel="stylesheet" type="text/css"/>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

    </head>

    <body>
        <header>
            <!-- The NavBar -->
            <nav class="navbar navbar-default navbar-fixed-top navbar-inverse" role="navigation">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-target">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>

                    </div>
                    <div class="collapse navbar-collapse" id="navbar-collapse-target">
                        <ul class="nav navbar-nav navbar-left">
                            <li><a href="/"><span class="glyphicon glyphicon-home"></span> Home</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Chapitres <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                    <li><a href="/chapters/last.html"><span class="glyphicon glyphicon-book"></span> Derniers chapitres publiée</a></li>
                                    <li><a href="/chapters/all.html"><span class="glyphicon glyphicon-book"></span> Tous les chapitres</a></li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
                            <?php if ($this->app->user()->isAdmin()) : ?>
                                <li class="active"><a href="/admin/home.html"><span class="glyphicon glyphicon-cog"></span> Administration</a></li>
                            <?php endif; ?>
                            <?php if (!$this->app->user()->isAuthenticated() && !$this->app->user()->isUser() && !$this->app->user()->isAdmin()) : ?>
                                <li><a href="/admin/connect.html"><span class="glyphicon glyphicon-log-in"></span> Se connecter</a></li>
                                <li><a href="/admin/user-insert.html"><span class="glyphicon glyphicon-user"></span> S'inscrire</a></li>
                            <?php endif; ?>
                            <?php if ($this->app->user()->isAuthenticated()) : ?>
                            <li><a href="/admin/disconnect.html"><span class="glyphicon glyphicon-unchecked"></span> Se deconnecter</a></li>

                            <?php endif; ?>
                        </ul>
                    </div>
                </div><!-- /.container -->
            </nav>
            
        </header>

        
        <div class="row" >
            <div class="jumbotron col-md-8 col-md-offset-2 col-xs-8 col-xs-offset-2" >
            </div>
        </div>
        
        <div id="main" class="container" >

            <!--Affichage des message flash si il y en a -->
            <?php if ($this->app->user()->hasFlash()) : ?>
                <div id="flashMessage" class=" alert alert-success">
                    <?= '<p class="text-center">' . $this->app->user()->getFlash() . '</p>'; ?>
                </div>
            <?php endif; ?>

            <!-- Affichage des erreurs si il y en a-->
			<?php if (isset($erreurs)) : ?>
                <div class="row">
                    <div class="alert alert-danger text-center">
                        <strong><?= $erreurs ?></strong>
                    </div>
                </div>
			<?php endif; ?>



            <?= $content ?>

        </div> <!-- bloc_content .container -->


        <!--  FOOTER   -->
        <div class="container-fluid">
            <div class="row">
                <footer class="footer">
                    <p class=" text-center">
                        <a href="http://www.github.com/freelance2608/CPM_DEV_P3_BlogMVC">This blog is a minimalist CMS  fully developed in Object Oriented PHP on an MVC architecture.</a>
                    </p>
                </footer>
            </div>
        </div>


        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://code.jquery.com/jquery-3.2.1.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
                crossorigin="anonymous"></script>
        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    </body>
</html>
