<?php
namespace App\Frontend\Modules\Comments;


use \Entity\Comments;
use \Form\FormBuilder\InsertCommentsFormBuilder;
use \OCFram\BackController;
use \OCFram\HTTPRequest;
use \Form\FormHandler;

class CommentsController extends BackController
{

    // INSERT A COMMENT
    public function executeInsert( HTTPRequest $request )
    {
        if( $request->method() == 'POST' ) {
            $comment = new Comments([
                'chapter' => $request->getData('id'),
                'author' => $request->postData('author'),
                'content' => $request->postData('content'),
            ]);

            if( $request->getExists( 'id' ) ) {
                $comment->setChapter( $request->getData( 'id' ) );
            }
        }
        else
        {
            $comment = new Comments;
        }

        $formBuilder = new InsertCommentsFormBuilder($comment);
        $formBuilder->build();
        $form = $formBuilder->form();

        $formHandler = new FormHandler( $form, $this->managers->getManagerOf( 'Comments' ), $request );

        if( $formHandler->process() ) {
            $this->app->user()->setFlash('Le commentaire a bien été ajouté, merci !' );
            $this->app->httpResponse()->redirect( '/chapters/chapter-'. $request->getData('id').'.html' );
        }

        // On passe les différentes variables à la vue
        $this->page->addVar( 'comment', $comment );
        $this->page->addVar( 'form', $form->createView() );
        $this->page->addVar( 'title', 'Ajout d\'un commentaire sur le chapitre ' . $_GET['id'] );
    }

    // ADD A FLAG To A COMMENT
    public function executeFlag( HTTPRequest $request )
    {
        $commentId = $request->getData( 'id' );
        $comment   = $this->managers->getManagerOf( 'Comments' )->find( $commentId );

        $flag = $comment[ 'flag' ];
        $flag++;

        $comment->setFlag($flag);

        $this->managers->getManagerOf( 'Comments' )->save($comment);

        $this->app->user()->setFlash( 'Le commentaire a bien été signalé !' );
        $this->app->httpResponse()->redirect( '/chapters/chapter-'.$comment->chapter() .'.html' );
    }

}