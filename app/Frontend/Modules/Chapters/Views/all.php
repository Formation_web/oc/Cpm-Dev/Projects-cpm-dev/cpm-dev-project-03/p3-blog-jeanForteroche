<hr>
<h2 class="text-center">Tous les <?= $nbChapters ?> chapitres </h2>

<hr>
<?php if(isset($chaptersList)): ?>

    <?php foreach ($chaptersList as $chapters): ?>

        <article class="col-md-8 col-md-offset-2 col-xs-8 col-xs-offset-2 panel panel-default">


            <h2 class="panel-heading text-center">
                <a href="/chapters/chapter-<?= $chapters['id'] ?>.html"><?= $chapters['title']; ?></a></h2>

            <div class="well">
                <p class="panel-body">
                    <?= nl2br($chapters['content']); ?>
                </p>
            </div>

            <div class="well">
                <p><a  class="btn btn-success pull-right " href="/chapters/chapter-<?= $chapters['id'] ?>.html">Lire la suite ...</a></p>
                <?php if($chapters['dateCreate'] != $chapters['lastModif']): ?>
                    <small class="text-center"><?= 'Modifié le ' . $chapters['lastModif']->format(' d/m/Y à H\hi'); ?></small>
                <?php else: ?>
                    <small class="text-center"><?= 'Publié le ' . $chapters['dateCreate']->format(' d/m/Y à H\hi');   ?></small>
                <?php endif; ?>

            </div>
        </article>

    <?php endforeach; ?>

<?php endif; ?>