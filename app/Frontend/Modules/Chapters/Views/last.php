    <hr>
        <h2 class="text-center">Les <?= $nbChapters ?> derniers chapitres </h2>
    <hr>

    <div class="row">

        <?php foreach ($chaptersList as $chapters): ?>

            <article class="col-md-8 col-md-offset-2 col-xs-8 col-xs-offset-2 panel panel-default">

                <h2 class="panel-heading text-center">
                    <a href="/chapters/chapter-<?= $chapters->id() ?>.html"><?= $chapters->title(); ?></a>
                </h2>

                <div class="well">
                    <p class="panel-body">
                        <?= nl2br($chapters->content()); ?>
                    </p>
                </div>

                <div class="well">
                    <p><a  class="btn btn-success pull-right" href="/chapters/chapter-<?= $chapters->id() ?>.html">Lire la suite ...</a></p>

                    <p class="text-center">
                        <?php if($chapters->dateCreate() != $chapters->lastModif()): ?>
                            <small><?= 'Modifié le ' . $chapters->lastModif()->format(' d/m/Y à H\hi'); ?></small>
                        <?php else: ?>
                            <small><?= 'Publié le ' . $chapters->dateCreate()->format(' d/m/Y à H\hi');   ?></small>
                        <?php endif; ?>
                    </p>
                </div>
            </article>

        <?php endforeach; ?>

    </div>
