<?php
namespace Model;

use \Entity\Comments;

/**
 * Class CommentsManagerPDO
 * @package Model
 */
class CommentsManagerPDO extends CommentsManager
{

    protected function insert( Comments $comment)
    {
        $q = $this->dao->prepare('INSERT INTO t_comment SET author = :author, content = :content, chapter = :chapter, dateCreate = NOW(), flag = 0' );

        $q->bindValue(':author', $comment->author());
        $q->bindValue(':chapter', $comment->chapter());
        $q->bindValue(':content', $comment->content());

        $q->execute();

//        $comment->setId($this->dao->lastInsertId());
    }

    public function getListOf( $chapter)
    {
        if (!ctype_digit($chapter))
        {
            throw new \InvalidArgumentException('L\'identifiant du chapitre passé doit être un nombre entier valide');
        }

        $q = $this->dao->prepare('SELECT * FROM t_comment WHERE chapter = :chapter');
        $q->bindValue(':chapter', $chapter, \PDO::PARAM_INT);
        $q->execute();

        $q->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Entity\Comments');

        $comments = $q->fetchAll();

        return $comments;
    }

    public function findAll()
    {
        $sql = 'SELECT * FROM t_comment';

        $requete = $this->dao->query($sql);
        $requete->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Entity\Comments');

        $commentsList = $requete->fetchAll();

        $requete->closeCursor();

        return $commentsList;

    }

    public function getList($debut = -1, $limite = -1)
    {
        $sql = "SELECT * FROM t_comment ";

        if($debut != -1 || $limite != -1)
        {
            $sql .= ' LIMIT ' . (int) $limite . ' OFFSET ' . (int) $debut;
        }

        $requete = $this->dao->query($sql);
        $requete->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Entity\Comments');

        $commentsList = $requete->fetchAll();

        foreach ($commentsList as $comment)
        {
            $comment->setDateCreate(new \DateTime($comment->dateCreate()));
        }

        $requete->closeCursor();

        return $commentsList;
    }

    public function find($id)
    {
        $q = $this->dao->prepare('SELECT * FROM t_comment WHERE id = :id');
        $q->bindValue(':id', (int) $id, \PDO::PARAM_INT);
        $q->execute();

        $q->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Entity\Comments');

        return $q->fetch();
    }

    public function delete($id)
    {
        $this->dao->exec('DELETE FROM t_comment WHERE id = '.(int) $id);
    }

    public function deleteAllByChapter($chapterId)
    {
        $this->dao->exec('DELETE FROM t_comment WHERE chapter = '.(int) $chapterId);
    }

    protected function update(Comments $comment)
    {
        $q = $this->dao->prepare('UPDATE t_comment SET  author = :author, chapter = :chapter, content = :content, flag = :flag WHERE id = :id');

        $q->bindValue(':chapter', $comment->chapter());
        $q->bindValue(':author', $comment->author());
        $q->bindValue(':content', $comment->content());
        $q->bindValue(':flag', $comment->flag());

        $q->bindValue(':id', $comment->id(), \PDO::PARAM_INT);

        $q->execute();

    }

    public function count()
    {
        return $this->dao->query('SELECT COUNT(*) FROM t_comment')->fetchColumn();
    }

    public function countCommentsSignaled()
    {
        return $this->dao->query('SELECT COUNT(*) FROM t_comment WHERE flag <> 0 ')->fetchColumn();
    }

    public function getListOfCommentsSignaled()
    {
        $sql = 'SELECT * FROM t_comment WHERE flag <> 0';

        $requete = $this->dao->query($sql);
        $requete->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Entity\Comments');

        $commentsListSignaled = $requete->fetchAll();

        $requete->closeCursor();

        return $commentsListSignaled;
    }

}