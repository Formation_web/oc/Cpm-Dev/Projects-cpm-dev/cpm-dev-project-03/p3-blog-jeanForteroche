<?php

namespace Model;

use \Entity\Chapters;
use \OCFram\Manager;

abstract class ChaptersManager extends Manager
{
    /**
     * Method for adding a chapter
     * @param $chapters Chapters The chapter to add.
     * @return void
     */
    abstract protected function insert(Chapters $chapters);

    /**
     * Save a chapter into the database
     * @param \Entity\Chapters $chapters The chapter to save.
     * @see self::insert()
     * @see self::update()
     * @return void
     */
    public function save(Chapters $chapters)
    {
        if ($chapters->isValid())
        {
            $chapters->isNew() ? $this->insert($chapters) : $this->update($chapters);
        }
        else
        {
            throw new \RuntimeException('Le chapitre doit être validée pour être enregistrée');
        }
    }

    /**
     * Returns a requested chapter list.
     * @param $debut int The first chapter to select.
     * @param $limite int The number of Chapters to select
     * @return array The list of Chapters. Each entry is an instance of Chapter.
     */
    abstract public function getList($debut = -1, $limite = -1);

    /**
     * Method returning a complete list of Chapters.
     * @return array The list of all Chapters. Each entry is an instance of Chapter.
     */
    abstract public function findAll();

    /**
     * Method returning a specific chapter.
     * @param $chap_id int The id of the chapter you want to find
     * @return Chapters The chapter ask
     */
    abstract public function find($id);

    /**
     * Returns the total number of Chapters.
     * @return int
     */
    abstract public function count();

    /**
     * Method for editing a chapter
     * @param $chapters Chapters The chapter to modify
     * @return void
     */
    abstract protected function update(Chapters $chapters);

    /**
     * Delete a chapter
     * @param $id integer The id of the chapter to delete
     * @return void
     */
    abstract public function delete($id);
    
}