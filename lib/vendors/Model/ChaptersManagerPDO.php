<?php

namespace Model;

use \Entity\Chapters;

class ChaptersManagerPDO extends ChaptersManager
{
    protected function insert(Chapters $chapters)
    {
        $requete = $this->dao->prepare('INSERT INTO t_chapter SET  id = :id, author = :author, title = :title, content = :content, dateCreate = NOW(), lastModif = NOW()');

        $requete->bindValue(':title', $chapters->title());
        $requete->bindValue(':author', $chapters->author());
        $requete->bindValue(':content', $chapters->content());

        $requete->bindValue(':id', $chapters->id());

        $requete->execute();
    }

    public function getList($debut = -1, $limite = -1)
    {
        $sql = "SELECT * FROM t_chapter ORDER BY id DESC";

        if($debut != -1 || $limite != -1)
        {
            $sql .= ' LIMIT ' . (int) $limite . ' OFFSET ' . (int) $debut;
        }

        $requete = $this->dao->query($sql);
        $requete->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Entity\Chapters');

        $chaptersList = $requete->fetchAll();

        foreach ($chaptersList as $chapters)
        {
            $chapters->setDateCreate(new \DateTime($chapters->dateCreate()));
            $chapters->setLastModif(new \DateTime($chapters->lastModif()));
        }

        $requete->closeCursor();

        return $chaptersList;
    }

    public function findAll()
    {
        $sql = 'SELECT * FROM t_chapter';

        $requete = $this->dao->query($sql);
        $requete->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, '\Entity\Chapters');

        $chaptersList = $requete->fetchAll();

        foreach ($chaptersList as $chapters)
        {
            $chapters->setDateCreate(new \DateTime($chapters->dateCreate()));
            $chapters->setLastModif(new \DateTime($chapters->lastModif()));
        }

        $requete->closeCursor();

        return $chaptersList;
    }

    public function find($id)
    {
        $requete = $this->dao->prepare('SELECT * FROM t_chapter WHERE id = :id');
        $requete->bindValue(':id', (int) $id, \PDO::PARAM_INT);
        $requete->execute();

        $requete->setFetchMode(\PDO::FETCH_CLASS | \PDO::FETCH_PROPS_LATE, 'Entity\Chapters');

        if($chapters = $requete->fetch())
        {
            $chapters->setDateCreate(new \DateTime($chapters->dateCreate()));
            $chapters->setLastModif(new \DateTime($chapters->lastModif()));

            return $chapters;
        }

        return null;
    }

    public function count()
    {
        return $this->dao->query('SELECT COUNT(*) FROM t_chapter')->fetchColumn();
    }

    protected function update(Chapters $chapters)
    {
        $requete = $this->dao->prepare('UPDATE t_chapter SET author = :author, title = :title, content = :content, lastModif = NOW() WHERE id = :id');

        $requete->bindValue(':title', $chapters->title());
        $requete->bindValue(':author', $chapters->author());
        $requete->bindValue(':content', $chapters->content());

        $requete->bindValue(':id', $chapters->id(), \PDO::PARAM_INT);

        $requete->execute();
    }

    public function delete($id)
    {
        $this->dao->exec('DELETE FROM t_chapter WHERE id = '.(int) $id);
    }

}