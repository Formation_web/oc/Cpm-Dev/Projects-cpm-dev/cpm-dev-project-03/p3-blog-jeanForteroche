<?php
namespace Model;

use \OCFram\Manager;
use \Entity\Comments;

abstract class CommentsManager extends Manager
{
    /**
     * Save a comment in the BDD
     * @param $comment Comments The comment to save
     * @return void
     */
    public function save(Comments $comment)
    {
        if ($comment->isValid())
        {
            $comment->isNew() ? $this->insert($comment) : $this->update($comment);
        }
        else
        {
            throw new \RuntimeException('Le commentaire doit être validée pour être enregistrée');
        }
    }

    /**
     * Insert a new Comment in BDD
     * @param $comment Comments The comment to insert
     * @return void
     */
    abstract protected function insert(Comments $comment);

    /**
     * Update a comment
     * @param $comment Comments The comment to update
     * @return void
     */
    abstract protected function update(Comments $comment);

    /**
     * Remove a comment from the database
     * @param integer $id The comment id.
     * @return void
     */
    abstract public function delete($id);

    /**
     * Remove all comments for a chapter
     * @param $chapterId Comments The chapter id
     * @return void
     */
    abstract public function deleteAllByChapter($chapterId);

    /**
     * Returns a comment matching the supplied id.
     * @param integer $id The comment id.
     * @return \Entity\Comments|throw an exception if no matching comment is found
     */
    abstract public function find($id);

    /**
     * Find all Comments for all Chapters
     * @return Comments
     */
    abstract public function findAll();

    /**
     * Méthode permettant de récupérer une liste de commentaires.
     * @param $chapters Comments Le chapitre sur lequelle on veut récupérer les commentaires
     * @return array
     */
    abstract public function getListOf($chapters);

    /**
     * Méthode permettant de récupérer le nombre totale de commentaires.
     * @return array
     */
    abstract public function count();

    /**
     * Méthode permettant de récupérer le nombre commentaires signalé.
     * @return array
     */
    abstract public function countCommentsSignaled();

    /**
     * Get all signaled comments
     * @return Comments
     */
    abstract public function getListOfCommentsSignaled();




}