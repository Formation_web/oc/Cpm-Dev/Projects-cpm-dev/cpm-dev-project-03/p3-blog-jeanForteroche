<?php

namespace Model;

use \Entity\Users;
use \OCFram\Manager;

abstract class UsersManager extends Manager
{
    /**
     * Insert a User in the BDD
     * @param $user Users The user to insert
     * @return void
     */
    abstract protected function insert(Users $user);

    /**
     * Save the user in the BDD
     * @param \Entity\Users $user The chapter to save.
     * @see self::insert()
     * @see self::update()
     * @return void
     */
    public function save(Users $user)
    {
        if ($user->isValid())
        {
            $user->isNew() ? $this->insert($user) : $this->update($user);
        }
        else
        {
            throw new \RuntimeException('L\'utilisateur doit être validée pour être enregistrée');
        }
    }

    /**
     * Return a user matching the supplied id..
     * @param integer $id The user id.
     * @return \Entity\Users |throw an exception if no matching user is found
     */
    abstract public function find($id);

    /**
     * Return a user matching the supplied username..
     * @param string $username The user username.
     * @return \Entity\Users |throw an exception if no matching user is found
     */
    abstract public function findByUsername($username);

    /**
     * Return a list of all users, sorted by role and name
     * @return array A list of all users.
     */
    abstract public function findAll();

    /**
     * Méthode renvoyant le nombre d'utilisateur total.
     * @return int
     */
    abstract public function count();

    /**
     * Méthode permettant de modifier un utilisateur.
     * @param $user Users L'utilisateur à modifier
     * @return void
     */
    abstract protected function update(Users $user);

    /**
     * Méthode permettant de supprimer un utilisateur.
     * @param $id int L'identifiant de l'utilisateur à supprimer
     * @return void
     */
    abstract public function delete($id);

    /**
     * Méthode permettant d'intervertir le rôle de l'utilisateur entre USER  || ADMIN
     * @param $id integer User Id
     * @return void
     */
    abstract public function switchUserRole($id);

    abstract public function checkUserForRegister($id, $username);


}