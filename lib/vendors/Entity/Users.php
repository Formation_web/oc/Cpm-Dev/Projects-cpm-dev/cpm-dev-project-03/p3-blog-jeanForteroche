<?php

namespace Entity;

use \OCFram\Entity;



class Users extends Entity
{

    /**
     * User id.
     * @var integer
     */
    protected $id;

    /**
     * User name.
     * @var string
     */
    protected $username;

    /**
     * User email
     * @var string
     */
    protected $email;

    /**
     * User Password.
     * @var string
     */
    protected $password;

    /**
     * Salt that was originally used to encode the password
     * @var string
     */
    protected $salt;

    /**
     * Date of register user
     * @var \DateTime
     */
    protected $inscription;

    /**
     * User role.
     * Values : ROLE_USER or ROLE_ADMIN
     * @var string
     */
    protected $role;

    const INVALID_USERNAME = 1;
    const INVALID_PASSWORD = 2;
    const INVALID_ROLE = 3;
    const INVALID_SALT = 4;
    const INVALID_EMAIL = 5;
    const INVALID_INSCRIPTION = 6;

    public function isValid()
    {
        return !(empty($this->username) || empty($this->password) || empty($this->salt) || empty($this->role));
    }

    // GETTERS //
    public function id()
    {
        return $this->id;
    }

    public function username()
    {
        return $this->username;
    }

    public function email()
    {
        return $this->email;
    }

    public function password()
    {
        return $this->password;
    }

    public function salt()
    {
        return $this->salt;
    }

    public function role()
    {
        return $this->role;
    }

    public function inscription()
    {
        return $this->inscription;
    }

    // SETTERS //
    public function setId($id)
    {
        if (is_integer($id) && $id > 0) {
            $this->id = $id;
        }
    }

    public function setUsername($username)
    {
        if (!is_string($username) || empty($username) || strlen($username) > 30) {
            $this->erreurs[] = self::INVALID_USERNAME;
        }

        $this->username = $username;
    }

    public function setEmail($email)
    {
        if (!is_string($email) || empty($email) ) {
            $this->erreurs[] = self::INVALID_EMAIL;
        }

        $this->email = $email;
    }

    public function setPassword($password)
    {
        if (!is_string($password) || empty($password)) {
            $this->erreurs[] = self::INVALID_PASSWORD;
        }

        $this->password = $password;
    }

    public function setSalt($salt)
    {
        if (!is_string($salt) || empty($salt)) {
            $this->erreurs[] = self::INVALID_SALT;
        }

        $this->salt = $salt;
    }

    public function setRole($role)
    {
        if (!is_string($role) || empty($role)) {
            $this->erreurs[] = self::INVALID_ROLE;
        }

        $this->role = $role;
    }

    public function setInscription( \DateTime $inscription)
    {
        if( !empty( $inscription ) ) {
            $this->inscription = $inscription;
        }
        else
        {
            $this->erreurs[] = self::INVALID_INSCRIPTION;
        }
        return $this;
    }


}