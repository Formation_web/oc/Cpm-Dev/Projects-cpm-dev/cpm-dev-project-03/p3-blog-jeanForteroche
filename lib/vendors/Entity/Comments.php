<?php
namespace Entity;

use DateTime;
use \OCFram\Entity;

/**
 * Class Comments
 * @package Entity
 */
class Comments extends Entity
{
    /**
     * Chapter Id;
     * @var integer
     */
    protected $chapter;

    /**
     * Comment author
     * @var string
     */
    protected $author;

    /**
     * Comment content
     * @var string
     */
    protected $content;

    /**
     * Comment date of creation
     * @var \DateTime
     */
    protected $dateCreate;

    /**
     * Comment flag
     * @var integer
     */
    protected $flag;


    const INVALID_AUTHOR = 1;
    const INVALID_CONTENT = 2;

    public function isValid()
    {
        return !(empty($this->author) || empty($this->content));
    }

    // SETTERS //
    public function setChapter($chapter)
    {
        $this->chapter = (int) $chapter;
    }

    public function setAuthor($author)
    {
        if (!is_string($author) || empty($author))
        {
            $this->erreurs[] = self::INVALID_AUTHOR;
        }

        $this->author = $author;

    }

    public function setContent($content)
    {
        if (!is_string($content) || empty($content))
        {
            $this->erreurs[] = self::INVALID_CONTENT;
        }

        $this->content = $content;
    }

    /**
     * @param \DateTime $dateCreate
     */
    public function setDateCreate(DateTime $dateCreate)
    {
        $this->dateCreate = $dateCreate;
    }

    public function setFlag( $flag)
    {
        $flag = (int) $flag;

        $this->flag = $flag;
    }



    // GETTERS //
    public function chapter()
    {
        return $this->chapter;
    }

    public function author()
    {
        return $this->author;
    }

    public function content()
    {
        return $this->content;
    }

    public function dateCreate()
    {
        return $this->dateCreate;
    }

    public function flag()
    {
        return (int) $this->flag;
    }


}