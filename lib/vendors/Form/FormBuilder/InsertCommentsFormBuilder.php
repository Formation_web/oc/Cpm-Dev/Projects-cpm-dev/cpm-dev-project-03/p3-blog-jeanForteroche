<?php
namespace Form\FormBuilder;

use Form\Field\HiddenField;
use \Form\Field\StringField;
use \Form\Field\TextField;
use \Form\Validators\MaxLengthValidator;
use \Form\Validators\NotNullValidator;

class InsertCommentsFormBuilder extends FormBuilder
{

    public function build()
    {
        $this->form->add(new HiddenField([
                      'label' => 'Chapitre associé : :',
                      'name' => 'chapter',
                      'value' => $_GET['id'],
                      'disabled' => 'disabled',
                      'validators' => [
                          new NotNullValidator('Merci de spécifier l\'auteur du commentaire'),
                      ],
                  ]))
            ->add(new StringField([
                'label' => 'Auteur :',
                'name' => 'author',
                'maxLength' => 30,
                'validators' => [
                    new MaxLengthValidator('L\'auteur spécifié est trop long (30 caractères maximum)', 30),
                    new NotNullValidator('Merci de spécifier l\'auteur du commentaire'),
                ],
            ]))
            ->add(new TextField([
                'label' => 'Message : ',
                'name' => 'content',
                'rows' => 7,
                'cols' => 50,
                'validators' => [
                    new NotNullValidator('Merci de spécifier votre commentaire'),
                ],
            ]));
    }
}