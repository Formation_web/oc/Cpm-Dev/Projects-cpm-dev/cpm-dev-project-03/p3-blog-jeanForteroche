<?php
namespace Form\FormBuilder;

use \Form\Field\PasswordField;
use \Form\Field\StringField;
use \Form\Field\EmailField;

use \Form\Validators\MaxLengthValidator;
use \Form\Validators\NotNullValidator;

class RegisterFormBuilder extends FormBuilder
{
    public function build()
    {
        $this->form->add(new StringField([
            'label' => 'Username :',
            'name' => 'username',
            'maxLength' => 40,
            'validators' => [
                new MaxLengthValidator('Le pseudo spécifié est trop long (30 caractères maximum)', 40),
                new NotNullValidator('Merci de spécifier le pseudo de l\'utilisateur'),
                ],
            ]))
            ->add(new EmailField([
            'label' => 'Email :',
            'name' => 'email',
            'validators' => [
                new NotNullValidator('Merci de spécifier une adresse email !'),
                ],
            ]))
            ->add(new PasswordField([
                'label' => 'Mot de passe :',
                'name' => 'password',
                'maxLength' => 88,
                'validators' => [
                    new MaxLengthValidator('Le mot de passe ne doit pas dépasser 88 caractères', 88),
                    new NotNullValidator('Merci de spécifier un mot de passe'),
                ],
            ]));

    }
}