<?php
namespace OCFram;


// session_start();

/**
 * Class User
 * @package OCFram
 */
class User extends ApplicationComponent
{
    /**
     * Get an attribute value
     * @param  $attr
     */
    public function getAttribute( $attr )
    {
        return isset( $_SESSION[ $attr ] ) ? $_SESSION[ $attr ] : null;
    }

    /**
     * Get the Flash message in array $_SESSION
     * @return mixed
     */
    public function getFlash()
    {
        $flash = $_SESSION[ 'flash' ];
        unset( $_SESSION[ 'flash' ] );

        return $flash;
    }

    /**
     * Verify if a flash message is present in $_SESSION
     * @return bool
     */
    public function hasFlash()
    {
        return isset( $_SESSION[ 'flash' ] );
    }

    /**
     * Verify if a User or Admin is logged
     * @return bool
     */
    public function isAuthenticated()
    {
        return isset( $_SESSION[ 'auth' ] ) && $_SESSION[ 'auth' ] === true;

    }

    /**
     * Verify if a ADMIN is logged
     * @return bool
     */
    public function isAdmin()
    {
        if( $this->isAuthenticated() ) {
            if( isset( $_SESSION[ 'role' ] ) && $_SESSION[ 'role' ] === 'ADMIN' ) {
                return true;
            } else {
                return false;
            }
        }

    }

    /**
     * Verify if a USER is logged
     * @return bool
     */
    public function isUser()
    {
        if( isset( $_SESSION[ 'auth' ] ) && $_SESSION[ 'auth' ] === true && isset( $_SESSION[ 'role' ] ) && $_SESSION[ 'role' ] === 'USER' ) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Add attribute to $_SESSION
     * @param $attr , $value
     * @param $value
     */
    public function setAttribute( $attr, $value )
    {
        $_SESSION[ $attr ] = $value;
    }

	/**
	 * Authenticate the user
	 * @param bool|Default $authenticated
	 */
	public function setAuthenticated( $authenticated )
	{
		if( $authenticated == false ) {
			$_SESSION[ 'auth' ] = $authenticated;
			$_SESSION[ 'role' ] = '';
		}
		if( $authenticated == true ) {
			$_SESSION[ 'auth' ] = $authenticated;
		}
	}


	/**
     * Add a flash message to $_SESSION
     * @param $value
     */
    public function setFlash( $value )
    {
        $_SESSION[ 'flash' ] = $value;
    }

}
